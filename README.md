# Jonathan Voth
## Master of Science in Analytics

**List the 3 Pig Queries you wrote.**

Query #1: query 01 = FILTER zip_income BY income < 50000;

Query #2: query02 = FILTER data BY income > 44382; ORDER query02 BY income ASC: (I did a previous statement that found the average income, then hard coded it into Query #2.)

Query #3: query03 = FILTER zip_income BY income < 25000 OR income > 75000; ORDER query03 BY zip;

**What technical errors did you experience? Please list them explicitly and identify how you corrected them.**

The first and really only issue I ran into was loading the data into Pig. I had correctly mounted my workspace but realized I wasn't running Pig in local mode. I tried to load from HDFS, but it took too long and timed out. I had to look up how to get local mode running. It turned out to be pretty easy. The command to do so is "Pig -x local". Then, the data loaded directly from my mounted workspace. Also, another small error I had was skipping the header line from the CSV file. For this, I just deleted it from the actual data file itself. This may or may not be the best solution, but it worked for me.

As always, I ran into simple syntax errors but managed those through trial and error.

**What conceptual difficulties did you experience?**

The one big looming question I have is why are there so many tools that basically do the same thing? I feel like both Hive and Pig are conceptually very similar. They both attempt to make MapReduce easier to code and faster to run. Yes, I know there are some differences between them, but still quite similar. From my perspective, a novice programmer, if that, I don't get it, and it makes it hard to know which is better or which to learn. I would rather spend a month learning one of them in depth than superficially learn two different things. I guess I just wish there was one tool that we all could learn and that was widely used.

This isn't really a conceptual difficulty but more of a personal opinion/frustration as I learn these different tools.

**How much time did you spend on each part of the assignment?**

Overall, I spent about between five hours on this assignment. The reading and video took about 45 minutes each. Once I was able to find the correct data path and load it into Pig, I had a very easy time running the queries. They took only a couple seconds to run. Storing the output was an extra step in the process but added very little time. Similar to last week, thinking of a third query to run took more time than the previous two queries combined. I ended up running a query that filtered out any zip codes and incomes that were either below $25,000 or above $75,000. The purpose behind this query was to see if the lower and higher income areas were of similar zip codes, possible meaning they were physically close geographically. Or, if we other data about unemployment rates or crime rates, we could compare to those areas.

Using Docker and Gitlab added minimal time to the assignment.

**What was the hardest part of this assignment?**

One of the hardest parts of this assignment was correctly uploading the data into Pig in the correct format. It was different from the tutorial video as we used local mode. I initially didn't do this and ran into the issues discussed previously. Also, again, I had somewhat of a hard time thinking of my own query to run. I wanted it to be somewhat different from last week but also have meaning. Additionally, I hard-coded the average income for Query #2. There is probably a way to do that in one single statement, but I couldn't figure it out. I tried to store the average in a relation 'avg_income', but Pig wouldn't let me use that in the subsequent statement.

**What was the easiest part of this assignment?**

The flow of Pig was easy for me to follow and understand. From my perspective, you create relations by using simple commands (i.e. FILTER, ORDER, FOREACH, etc.), and then that relation is saved and you can manipulate it further if needed. I can see how this makes sense to programmers and why they like to use Pig as opposed to Hive or another tool. I also found it easy to store the output of each query. It was more difficult in Hive, but for Pig, you just need to use the 'STORE' command and the path to the directly. Because we were in local mode, the directory was the workspacee we mounted. Overall, I was able to navigate Pig quite easily.

**What advice would you give someone doing this assignment in the future?**

I found the videos helpful this week. I watched the demonstrations around 2-3 times. This allowed me to understand how Pig works. For me, I find examples very helpful when learning a new task or skill. In addition to the videos provided, I found a couple of other resources that walked through step by step on how to perform certain queries in Pig. Overall, my advice would be to find how you learn best and search for resources that fit that learning style. For me, it was the videos. For others, it could be something else and not everyone is the same. Know the best way you learn and retain information and use that method.

**What did you actually learn from doing this assignment?**

This week, I learned about Pig. Pig is a platform that execute MapReduce jobs in a language called Pig Latin. I wonder why it is called Pig and Pig Latin. What's the story behind that? Anyway, Pig is similar to Hive, but there are some major differences too. Pig can use both unstructured and structured data whereas Hive can deal with structured data. Pig Latin, the language used in Pig, is a data flow language. The user can define how the data from each query should be read and processed. I found this very convenient when writing the queries for this assignment.

**Why does what I learned matter both academically and practically?**

As I mentioned on the last reflection, I am in the process of applying for jobs. Actually, before this week, I came across an application and one of the qualifications was experience in Pig and Pig Latin. I immediately skipped over the job because I had no idea what that was and clearly had never learned if before. Now, after doing this assignment, I know what Pig is. I checked another box. In order to be an attractive candidate, I feel it is best that we know a little about a lot versus the other way around. I want to get exposed to as many tools and platforms as possible because different employers use different things. I can do in-depth learning of Hive or Pig once I get hired and know the specific tool they use. For now, I am glad to have had exposure to Pig.